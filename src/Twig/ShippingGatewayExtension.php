<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ShippingPlugin\Twig;

use BitBag\SyliusShippingExportPlugin\Repository\ShippingGatewayRepositoryInterface;

class ShippingGatewayExtension extends \Twig_Extension
{
    /**
     * @var ShippingGatewayRepositoryInterface
     */
    private $shippingGatewayRepository;
    /**
     * {@inheritdoc}
     */
    public function __construct(ShippingGatewayRepositoryInterface $shippingGatewayRepository)
    {
        $this->shippingGatewayRepository = $shippingGatewayRepository;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction(
                'omni_get_shipping_gateways',
                [
                    $this,
                    'getShippingGateways',
                ]
            ),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getShippingGateways()
    {
        return $this->shippingGatewayRepository->findAll();
    }
}
