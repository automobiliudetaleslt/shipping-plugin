<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ShippingPlugin\Form;

use Omni\Sylius\ShippingPlugin\Entity\ShipperConfig;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SenderShipperConfigType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'config',
                AbstractShipperConfigType::class,
                [
                    'required' => false,
                ]
            )
            ->add(
                'gateway',
                HiddenType::class
            );

        // @TODO: use default symfony transformer as for now it removes all values from fields
        $builder->get('config')->addModelTransformer(new CallbackTransformer(
            function ($configValues) {
                return $configValues;
            },
            function ($configValues) {
                foreach ($configValues as $key => $value) {
                    if ($value === null || is_array($value) && empty(array_filter($value))) {
                        unset($configValues[$key]);
                    }
                }

                return $configValues;
            })
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => \App\Entity\Shipping\ShipperConfig::class,
            ]
        );
    }
}
