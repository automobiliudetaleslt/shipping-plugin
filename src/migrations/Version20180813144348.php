<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Sylius\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180813144348 extends AbstractMigration
{
    const SHIPPING_PICKUP_CODE = 'pickup';

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }

    /**
     * @param Schema $schema
     * @return bool|void
     * @throws \Doctrine\DBAL\DBALException
     */
    public function postUp(Schema $schema)
    {
        $this->createShippingMethod();

        $shippingMethodId = $this->getPickupMethodId();

        $this->createShippingMethodChannels($shippingMethodId);
        $this->createShippingMethodTranslation($shippingMethodId);
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    public function postDown(Schema $schema)
    {
        $shippingMethodId = $this->getPickupMethodId();

        if ($shippingMethodId) {
                $this->removeShippingMethodTranslation($shippingMethodId);
                $this->removeShippingMethodChannels($shippingMethodId);
                $this->removeShippingMethod($shippingMethodId);
        }
    }

    /**
     * @return int
     * @throws \Doctrine\DBAL\DBALException
     */
    private function createShippingMethod()
    {
        $dateTime = new \DateTime();

        return $this->connection->insert(
            'sylius_shipping_method',
            [
                'code' => self::SHIPPING_PICKUP_CODE,
                'zone_id' => 2,
                'configuration' => serialize([
                    'b2c' => [
                        'amount' => 0,
                    ],
                    'b2b' => [
                        'amount' => 0,
                    ],
                ]),
                'category_requirement' => 1,
                'calculator' => 'flat_rate',
                'is_enabled' => 1,
                'position' => 3,
                'created_at' => $dateTime->format('Y-m-d H:i:s'),
                'updated_at' => $dateTime->format('Y-m-d H:i:s'),

            ]
        );
    }

    /**
     * @param int $shippingMethodId
     * @return null
     * @throws \Doctrine\DBAL\DBALException
     */
    private function createShippingMethodChannels($shippingMethodId)
    {
        $queryBuilder = $this->connection->createQueryBuilder();
        $queryBuilder
            ->select('syliusChannel.id')
            ->from('sylius_channel', 'syliusChannel');

        $channels = $queryBuilder->execute();

        if (count($channels) === 0) {
            return null;
        }

        foreach ($channels as $channel) {
            $this->connection->insert(
                'sylius_shipping_method_channels',
                [
                    'shipping_method_id' => $shippingMethodId,
                    'channel_id' => $channel['id'],
                ]
            );
        }
    }

    /**
     * @param int $shippingMethodId
     * @throws \Doctrine\DBAL\DBALException
     */
    private function createShippingMethodTranslation($shippingMethodId)
    {
        $this->connection->insert(
            'sylius_shipping_method_translation',
            [
                'name' => 'Pick up location',
                'translatable_id' => $shippingMethodId,
                'locale' => 'en_US',
            ]
        );
    }

    /**
     * @return null|int
     */
    private function getPickupMethodId()
    {
        $queryBuilder = $this->connection->createQueryBuilder();
        $queryBuilder
            ->select('shippingMethod.id')
            ->from('sylius_shipping_method', 'shippingMethod')
            ->where('shippingMethod.code = :code')
            ->setParameter('code', self::SHIPPING_PICKUP_CODE)
            ->getSQL();

        $methods = $queryBuilder->execute();

        if (count($methods) === 0) {
            return null;
        }

        foreach ($methods as $method) {
            return $method['id'];
        }

        return null;
    }

    /**
     * @param int $shippingMethodId
     * @return int
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    private function removeShippingMethodTranslation($shippingMethodId)
    {
        return $this->connection->delete(
            'sylius_shipping_method_translation',
            [
                'translatable_id' => $shippingMethodId,
            ]
        );
    }

    /**
     * @param int $shippingMethodId
     * @return int
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    private function removeShippingMethodChannels($shippingMethodId)
    {
        return $this->connection->delete(
            'sylius_shipping_method_channels',
            [
                'shipping_method_id' => $shippingMethodId,
            ]
        );
    }

    /**
     * @param int $shippingMethodId
     * @return int
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     */
    private function removeShippingMethod($shippingMethodId)
    {
        return $this->connection->delete(
            'sylius_shipping_method',
            [
                'id' => $shippingMethodId,
            ]
        );
    }
}
